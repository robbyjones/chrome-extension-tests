var firebaseConfig = {
    apiKey: "AIzaSyC7knLnuDQkYljW4s5OVCZGcVGVAEyE9rA",
    authDomain: "pointy-20c9b.firebaseapp.com",
    projectId: "pointy-20c9b",
    storageBucket: "pointy-20c9b.appspot.com",
    messagingSenderId: "449212828035",
    appId: "1:449212828035:web:7fa8937cb4677eaaca0eb6",
    measurementId: "G-F80RLCPGBT"
  };
  // Initialize Firebase
firebase.initializeApp(firebaseConfig);

console.log(firebase);

chrome.runtime.onMessage.addListener((msg, sender, response) => {
  
  if(msg.command == "fetch"){
    var domain = msg.data.domain;
    console.log('domain:', domain);
    var enc_domain = btoa(domain);
    firebase.database().ref('/domain/'+enc_domain).once('value').then(function(snapshot){
      response({type: "result", status: "success", data: snapshot.val(), request: msg});

    });
  }

  if(msg.command == "post"){

    var domain = msg.data.domain;
    var enc_domain = btoa(domain);
    var code = msg.data.code;
    var desc = msg.data.desc;

    try{

      var newPost = firebase.database().ref('/domain/'+enc_domain).push().set({
        code: code,
        description: desc
      });

      var postId = newPost.key;
      response({type: "result", status: "success", data: postId, request: msg});

    }catch(e){
      console.log("error:", e);
      response({type: "result", status: "error", data: e, request: msg});

    }
  }

  return true;

})
